package com.aveljanov.weatherapplication.core.service.weather

import com.aveljanov.weatherapplication.core.model.GeoDirection
import com.aveljanov.weatherapplication.core.model.Units
import com.aveljanov.weatherapplication.core.model.WeatherResponse
import com.aveljanov.weatherapplication.core.service.units.UnitsPrefsStorage
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class WeatherDisplayDataServiceImplTest {

    private lateinit var weatherDisplayDataService: WeatherDisplayDataService

    @MockK
    private lateinit var unitsPrefsStorage: UnitsPrefsStorage

    private lateinit var weatherResponse: WeatherResponse

    private val jsonResponse =
        "{\"coord\":{\"lon\":-0.13,\"lat\":51.51},\"weather\":[{\"id\":300,\"main\":\"Drizzle\",\"description\":\"light intensity drizzle\",\"icon\":\"09d\"}],\"base\":\"stations\",\"main\":{\"temp\":280.32,\"pressure\":1012,\"humidity\":81,\"temp_min\":279.15,\"temp_max\":281.15},\"visibility\":10000,\"wind\":{\"speed\":4.1,\"deg\":80},\"clouds\":{\"all\":90},\"dt\":1485789600,\"sys\":{\"type\":1,\"id\":5091,\"message\":0.0103,\"country\":\"GB\",\"sunrise\":1485762037,\"sunset\":1485794875},\"id\":2643743,\"name\":\"London\",\"cod\":200}"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        weatherResponse = Json.decodeFromString(WeatherResponse.serializer(), jsonResponse)
        weatherDisplayDataService = WeatherDisplayDataServiceImpl(unitsPrefsStorage)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun convertToDisplayDataModel() = runBlocking {
        every { runBlocking { unitsPrefsStorage.getUnits() } } returns Units.METRIC
        val displayModel =
            weatherDisplayDataService.convertToDisplayDataModel(weatherResponse, null)
        assertEquals("London,GB", displayModel?.place)
        assertEquals("Drizzle", displayModel?.weatherConditionDescription)
        assertEquals(280, displayModel?.temperature)
        assertEquals(1012, displayModel?.pressure)
        assertEquals(81, displayModel?.humidity)
        assertEquals(279, displayModel?.minTemp)
        assertEquals(281, displayModel?.maxTemp)
        assertEquals(4.1f, displayModel?.windSpeed)
        assertEquals(GeoDirection.NE, displayModel?.windDirection)
        assertEquals(90, displayModel?.cloudsPercentage)
    }
}