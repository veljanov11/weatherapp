package com.aveljanov.weatherapplication.core.service.units

import android.content.SharedPreferences
import com.aveljanov.weatherapplication.core.model.Units
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class UnitsPrefsStorageImplTest {


    private lateinit var unitsPrefsStorage: UnitsPrefsStorage

    @MockK
    private lateinit var mockSharedPreferences: SharedPreferences

    @MockK
    private lateinit var mockEditor: SharedPreferences.Editor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        every {
            mockSharedPreferences.edit()
        } returns mockEditor
        unitsPrefsStorage = UnitsPrefsStorageImpl(mockSharedPreferences)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun getUnits() = runBlocking {
        every {
            mockSharedPreferences.getString(
                "units",
                Units.METRIC.name
            )
        } returns Units.METRIC.name
        var units = unitsPrefsStorage.getUnits()
        assertEquals(Units.METRIC, units)

        every {
            mockSharedPreferences.getString(
                "units",
                Units.METRIC.name
            )
        } returns Units.IMPERIAL.name
        units = unitsPrefsStorage.getUnits()
        assertEquals(Units.IMPERIAL, units)
    }
}