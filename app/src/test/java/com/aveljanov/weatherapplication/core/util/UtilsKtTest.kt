package com.aveljanov.weatherapplication.core.util

import com.aveljanov.weatherapplication.core.model.GeoDirection
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class UtilsKtTest {

    @Test
    fun convertToTimeStringText() {
        val calendar = Calendar.getInstance().apply {
            set(2020, Calendar.APRIL, 4, 12, 44)
        }
        assertEquals("12:44", convertToTimeString(calendar.time))
    }

    @Test
    fun convertDegreesToGeoDirectionTest() {

        assertEquals(GeoDirection.E, convertDegreesToGeoDirection(0))
        assertEquals(GeoDirection.N, convertDegreesToGeoDirection(90))
        assertEquals(GeoDirection.W, convertDegreesToGeoDirection(180))
        assertEquals(GeoDirection.S, convertDegreesToGeoDirection(270))

        assertEquals(GeoDirection.NE, convertDegreesToGeoDirection(1))
        assertEquals(GeoDirection.NW, convertDegreesToGeoDirection(91))
        assertEquals(GeoDirection.SW, convertDegreesToGeoDirection(181))
        assertEquals(GeoDirection.SE, convertDegreesToGeoDirection(271))

        assertEquals(GeoDirection.NE, convertDegreesToGeoDirection(89))
        assertEquals(GeoDirection.NW, convertDegreesToGeoDirection(179))
        assertEquals(GeoDirection.SW, convertDegreesToGeoDirection(269))
        assertEquals(GeoDirection.SE, convertDegreesToGeoDirection(359))

    }

}