package com.aveljanov.weatherapplication.ui.current_weather.model

import android.graphics.Bitmap
import com.aveljanov.weatherapplication.core.model.GeoDirection
import com.aveljanov.weatherapplication.core.model.Units
import java.util.*

data class WeatherDisplayModel(
    val place: String?,
    val weatherConditionDescription: String?,
    val weatherConditionIcon: Bitmap?,
    val temperature: Int?,
    val realFeelTemp: Int?,
    val minTemp: Int?,
    val maxTemp: Int?,
    val humidity: Int?,
    val pressure: Int?,
    val sunriseTime: Date?,
    val sunsetTime: Date?,
    val isAfterSunset: Boolean,
    val windSpeed: Float?,
    val windDirection: GeoDirection?,
    val cloudsPercentage: Int?,
    val visibility: Int?,
    val units: Units
)