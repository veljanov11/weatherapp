package com.aveljanov.weatherapplication.ui.current_weather

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.aveljanov.weatherapplication.R
import com.aveljanov.weatherapplication.core.model.Units
import com.aveljanov.weatherapplication.core.util.*
import com.aveljanov.weatherapplication.ui.current_weather.model.WeatherDisplayModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_current_weather.*
import kotlinx.android.synthetic.main.layout_details_section.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class CurrentWeatherFragment : Fragment() {

    companion object {
        private const val PERM_RC = 123
    }

    private val currentWeatherViewModel: CurrentWeatherViewModel by viewModel()

    private val rationaleAlertDialog by lazy {
        AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.location_rationale_message))
            .setPositiveButton(R.string.ok) { dialog, which ->
                requestLocationPermission()
                dialog.dismiss()
            }.setNegativeButton(R.string.cancel) { dialog, which ->
                dialog.dismiss()
            }.setCancelable(false).create()
    }

    private val settingsDialog by lazy {
        AlertDialog.Builder(requireContext())
            .setMessage(R.string.location_rationale_message)
            .setPositiveButton(R.string.go_to_settings) { dialog, which ->
                goToLocationSettings()
                dialog.dismiss()
            }.setNegativeButton(R.string.cancel) { dialog, which ->
                dialog.cancel()
            }.create()
    }

    private val snackbar by lazy {
        Snackbar.make(requireView(), "", Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok) { v -> }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(requireContext())
            .inflate(R.layout.fragment_current_weather, container)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_actionbar, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchView = menu.getItem(0).actionView as SearchView
        searchView.queryHint = getString(R.string.city_example)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                currentWeatherViewModel.loadWeatherForCity(query)
                searchView.isIconified = true
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        menu.getItem(1).setOnMenuItemClickListener {
            currentWeatherViewModel.loadCurrentLocationWeather()
            true
        }
    }

    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        currentWeatherViewModel.checkLocationPermissionMessage.observe(
            viewLifecycleOwner,
            Observer<Boolean> {
                it.isTrue {
                    snackbar.setText(R.string.check_location_message).show()
                }
                currentWeatherViewModel.checkLocationPermissionMessage.postValue(false)
                swipeToRefresh.isRefreshing = false
            })
        currentWeatherViewModel.errorMessage.observe(
            viewLifecycleOwner,
            Observer<String?> {
                it?.takeUnless { it.isBlank() }?.let {
                    snackbar.setText(getString(R.string.error, it)).show()
                }
                swipeToRefresh.isRefreshing = false
            }
        )
        currentWeatherViewModel.weatherLiveData.observe(
            viewLifecycleOwner,
            Observer<WeatherDisplayModel?> {
                it?.let { model ->
                    placeTextView.text = model.place
                    weatherConditionsTextView.text =
                        "${model.weatherConditionDescription}, ${getString(
                            R.string.real_feel,
                            getTemperatureString(model.realFeelTemp)
                        )
                        }"
                    let { tempTextView.text = getTemperatureString(model.temperature) }
                    minTempTextView.text = getTemperatureString(model.minTemp)
                    maxTempTextView.text = getTemperatureString(model.maxTemp)
                    val windString =
                        if (model.units == Units.METRIC) R.string.wind_description_metric else R.string.wind_description_imperial
                    windSection.valueTextView.text =
                        getString(windString, model.windSpeed, model.windDirection)
                    humiditySection.valueTextView.text =
                        getString(R.string.percentage_value, model.humidity)
                    pressureSection.valueTextView.text =
                        getString(R.string.pressure_value, model.pressure)
                    cloudySection.valueTextView.text =
                        getString(R.string.percentage_value, model.cloudsPercentage)
                    sunriseSection.valueTextView.text = convertToTimeString(model.sunriseTime)
                    sunsetSection.valueTextView.text = convertToTimeString(model.sunsetTime)
                    conditionImageView.setImageBitmap(model.weatherConditionIcon)
                }
                swipeToRefresh.isRefreshing = false
            })

        currentWeatherViewModel.units.observe(viewLifecycleOwner, Observer<Units> {
            when (it) {
                Units.IMPERIAL -> toggleGroup.check(R.id.fahrenheitButton)
                else -> toggleGroup.check(R.id.celsiusButton)
            }
        })

        currentWeatherViewModel.isLoading.observe(viewLifecycleOwner, Observer<Boolean> {
            it.isTrue { progressBar.show() }.isFalse { progressBar.hide() }
        })

        celsiusButton.setOnClickListener {
            currentWeatherViewModel.toggleUnits()
        }

        fahrenheitButton.setOnClickListener {
            currentWeatherViewModel.toggleUnits()
        }

        currentWeatherViewModel.initialize()

        swipeToRefresh.setOnRefreshListener {
            currentWeatherViewModel.refresh()
        }

        api23 {
            val granted =
                requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED
            granted.isTrue {
                currentWeatherViewModel.loadCurrentLocationWeather()
            }.isFalse {
                requestLocationPermission()
            }
        } ?: let {
            currentWeatherViewModel.loadCurrentLocationWeather()
        }
    }

    private fun requestLocationPermission() {
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERM_RC
        )
    }

    private fun getTemperatureString(temperature: Int?) =
        getString(R.string.temperature, temperature)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        (requestCode == PERM_RC).isTrue {
            grantResults.any { it == PERMISSION_GRANTED }.isTrue {
                currentWeatherViewModel.loadCurrentLocationWeather()
            }.isFalse {
                val showRationale =
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                showRationale.isTrue {
                    rationaleAlertDialog.takeIf { !it.isShowing }?.show()
                }.isFalse {
                    settingsDialog.takeIf { !it.isShowing }?.show()
                }
            }
        }
    }

    private fun goToLocationSettings() {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + requireActivity().packageName)
        )
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

}
