package com.aveljanov.weatherapplication.ui.current_weather

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aveljanov.weatherapplication.core.model.Units
import com.aveljanov.weatherapplication.core.model.WeatherResponse
import com.aveljanov.weatherapplication.core.service.location.LocationService
import com.aveljanov.weatherapplication.core.service.result.AppResult
import com.aveljanov.weatherapplication.core.service.units.UnitsPrefsStorage
import com.aveljanov.weatherapplication.core.service.weather.WeatherDisplayDataService
import com.aveljanov.weatherapplication.core.service.weather.WeatherService
import com.aveljanov.weatherapplication.core.util.isFalse
import com.aveljanov.weatherapplication.core.util.isTrue
import com.aveljanov.weatherapplication.ui.current_weather.model.WeatherDisplayModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class CurrentWeatherViewModel(
    private val uiScope: CoroutineScope,
    private val ioScope: CoroutineScope,
    private val weatherService: WeatherService,
    private val weatherDisplayDataService: WeatherDisplayDataService,
    private val unitsPrefsStorage: UnitsPrefsStorage,
    private val locationService: LocationService
) : ViewModel() {

    val weatherLiveData = MutableLiveData<WeatherDisplayModel>(null)
    val errorMessage = MutableLiveData<String?>(null)
    val units = MutableLiveData<Units>()
    val isLoading = MutableLiveData<Boolean>(false)
    val checkLocationPermissionMessage = MutableLiveData(false)

    private var isAutoLocation = true
    private var lastQuery: String? = null

    fun loadCurrentLocationWeather() {
        isLoading.postValue(true)
        isAutoLocation = true
        locationService.getCurrentLocation { deviceLocation ->
            uiScope.launch {
                deviceLocation?.let {
                    checkLocationPermissionMessage.postValue(false)
                    val deferred = ioScope.async {
                        weatherService.getWeatherForLocation(deviceLocation)
                    }
                    handleWeatherResponseResult(deferred.await())
                } ?: let {
                    checkLocationPermissionMessage.postValue(true)
                }
            }
        }

    }

    fun loadWeatherForCity(query: String?) {
        isLoading.postValue(true)
        isAutoLocation = false
        lastQuery = query
        query?.let {
            uiScope.launch {
                val deferred = ioScope.async {
                    weatherService.getWeatherForCity(query)
                }
                handleWeatherResponseResult(deferred.await())
            }
        }
    }

    private suspend fun handleWeatherResponseResult(result: AppResult<WeatherResponse>) {
        when (result) {
            is AppResult.Success<WeatherResponse> -> {
                val icons = ioScope.async {
                    result.data?.weather?.map {
                        weatherService.getIcon(it.icon)
                    }
                }.await()
                val displayModel =
                    weatherDisplayDataService.convertToDisplayDataModel(result.data, icons?.get(0))
                weatherLiveData.postValue(displayModel)
            }
            is AppResult.Failure -> errorMessage.postValue(result.throwable?.message)
        }
        isLoading.postValue(false)
    }

    fun toggleUnits() {
        ioScope.launch {
            val newUnits = when (units.value) {
                Units.METRIC -> Units.IMPERIAL
                Units.IMPERIAL -> Units.METRIC
                else -> Units.METRIC
            }
            unitsPrefsStorage.setUnits(newUnits)
            units.postValue(newUnits)
            refresh()
        }
    }

    fun refresh() {
        isAutoLocation.isTrue {
            loadCurrentLocationWeather()
        }.isFalse {
            lastQuery?.let {
                loadWeatherForCity(lastQuery)
            } ?: let {
                loadCurrentLocationWeather()
            }
        }
    }

    fun initialize() {
        ioScope.launch {
            units.postValue(unitsPrefsStorage.getUnits())
        }
    }
}