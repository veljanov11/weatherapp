package com.aveljanov.weatherapplication.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.aveljanov.weatherapplication.R
import kotlinx.android.synthetic.main.layout_details_section.view.*

class DetailsSection @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttrSet: Int = 0,
    refStyleId: Int = 0
) : FrameLayout(context, attrs, defStyleAttrSet, refStyleId) {

    init {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.DetailsSection)
        val title = attributes.getString(R.styleable.DetailsSection_title) ?: ""
        val icon = attributes.getDrawable(R.styleable.DetailsSection_icon)

        LayoutInflater.from(context).inflate(R.layout.layout_details_section, this).apply {
            sectionTitleTextView.text = title
            sectionTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                icon,
                null,
                null,
                null
            )
        }

        attributes.recycle()
    }

}