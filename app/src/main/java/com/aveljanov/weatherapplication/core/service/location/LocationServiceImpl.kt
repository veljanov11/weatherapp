package com.aveljanov.weatherapplication.core.service.location

import android.annotation.SuppressLint
import android.location.Location
import com.aveljanov.weatherapplication.core.model.DeviceLocation
import com.google.android.gms.location.FusedLocationProviderClient

class LocationServiceImpl(private val fusedLocationProviderClient: FusedLocationProviderClient) : LocationService {
    @SuppressLint("MissingPermission")
    override fun getCurrentLocation(onLocation : (location: DeviceLocation?) -> Unit) {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { it: Location? ->
            onLocation(it?.let { DeviceLocation(it.latitude, it.longitude) })
        }
    }
}