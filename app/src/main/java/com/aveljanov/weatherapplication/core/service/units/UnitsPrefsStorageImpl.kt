package com.aveljanov.weatherapplication.core.service.units

import android.content.SharedPreferences
import com.aveljanov.weatherapplication.core.model.Units

class UnitsPrefsStorageImpl(private val sharedPreferences: SharedPreferences) : UnitsPrefsStorage {

    companion object {
        private const val KEY = "units"
    }

    override suspend fun getUnits(): Units {
        return when (sharedPreferences.getString(KEY, Units.METRIC.name)) {
            Units.METRIC.name -> Units.METRIC
            Units.IMPERIAL.name -> Units.IMPERIAL
            else -> Units.METRIC
        }
    }

    override suspend fun setUnits(units: Units) {
        val value = when (units) {
            Units.METRIC -> Units.METRIC.name
            Units.IMPERIAL -> Units.IMPERIAL.name
        }
        sharedPreferences.edit().putString(KEY, value).apply()
    }
}