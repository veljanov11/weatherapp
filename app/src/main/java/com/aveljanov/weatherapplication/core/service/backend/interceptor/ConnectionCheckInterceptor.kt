package com.aveljanov.weatherapplication.core.service.backend.interceptor

import android.net.ConnectivityManager
import com.aveljanov.weatherapplication.core.service.stringprovider.StringProviderService
import com.aveljanov.weatherapplication.core.service.stringprovider.StringProviderServiceImpl
import com.aveljanov.weatherapplication.core.util.hasConnection
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Throws a [ConnectionException] if there is no connection active.
 */
class ConnectionCheckInterceptor(
    private val connectivityManager: ConnectivityManager,
    private val stringProviderService: StringProviderService
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return connectivityManager.hasConnection().takeIf { it }
            ?.let { chain.proceed(chain.request()) } ?: throw ConnectionException(
            stringProviderService.getString(StringProviderServiceImpl.CHECK_NETWORK_CONNECTION)
                ?: ""
        )
    }
}