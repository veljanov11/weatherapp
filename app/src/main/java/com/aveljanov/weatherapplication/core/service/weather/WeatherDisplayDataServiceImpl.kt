package com.aveljanov.weatherapplication.core.service.weather

import android.graphics.Bitmap
import com.aveljanov.weatherapplication.core.model.Sys
import com.aveljanov.weatherapplication.core.model.Units
import com.aveljanov.weatherapplication.core.model.WeatherCondition
import com.aveljanov.weatherapplication.core.model.WeatherResponse
import com.aveljanov.weatherapplication.core.service.units.UnitsPrefsStorage
import com.aveljanov.weatherapplication.core.util.convertDegreesToGeoDirection
import com.aveljanov.weatherapplication.core.util.convertUTCMillsToDate
import com.aveljanov.weatherapplication.ui.current_weather.model.WeatherDisplayModel
import java.util.*
import kotlin.math.roundToInt

class WeatherDisplayDataServiceImpl(private val unitsPrefsStorage: UnitsPrefsStorage) :
    WeatherDisplayDataService {

    private lateinit var units: Units

    override suspend fun convertToDisplayDataModel(
        weatherResponse: WeatherResponse?,
        iconBitmap: Bitmap?
    ): WeatherDisplayModel? {
        return weatherResponse?.let {
            units = unitsPrefsStorage.getUnits()
            val place = getPlace(weatherResponse.name, weatherResponse.sys)
            val conditionsDescription = getConditionsDescription(weatherResponse.weather)
            val temperature = weatherResponse.main?.temp?.roundToInt()
            val realFeelTemp = weatherResponse.main?.feelsLike?.roundToInt()
            val maxTemp = weatherResponse.main?.tempMax?.roundToInt()
            val minTemp = weatherResponse.main?.tempMin?.roundToInt()
            val pressure = weatherResponse.main?.pressure
            val humidity = weatherResponse.main?.humidity
            val sunrise = convertUTCMillsToDate(
                weatherResponse.sys?.sunrise,
                weatherResponse.timezone?.toLong()
            )
            val sunset = convertUTCMillsToDate(
                weatherResponse.sys?.sunset,
                weatherResponse.timezone?.toLong()
            )
            val isAfterSunset = isAfterSunset(sunset, weatherResponse.timezone)
            val windSpeed = weatherResponse.wind?.speed
            val windDegrees = weatherResponse.wind?.deg
            val cloudsPercentage = weatherResponse.clouds?.all
            return WeatherDisplayModel(
                place = place,
                weatherConditionDescription = conditionsDescription,
                weatherConditionIcon = iconBitmap,
                temperature = temperature,
                realFeelTemp = realFeelTemp,
                minTemp = minTemp,
                maxTemp = maxTemp,
                pressure = pressure,
                humidity = humidity,
                sunriseTime = sunrise,
                sunsetTime = sunset,
                isAfterSunset = isAfterSunset,
                windSpeed = windSpeed,
                windDirection = convertDegreesToGeoDirection(windDegrees),
                cloudsPercentage = cloudsPercentage,
                visibility = weatherResponse.visibility?.toInt(),
                units = units
            )
        }
    }

    private fun getConditionsDescription(weather: List<WeatherCondition>?): String? {
        return weather?.fold("", { s: String, weatherCondition: WeatherCondition ->
            s.plus(weatherCondition.main)
        })
    }

    private fun isAfterSunset(sunset: Date?, timezone: Double?): Boolean =
        timezone?.let {
            val instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            instance.add(Calendar.SECOND, timezone.toInt())
            sunset?.before(instance.time)
        } ?: false

    private fun getPlace(name: String?, sys: Sys?): String? =
        name?.let { name ->
            "$name${sys?.country?.takeIf { it.isNotBlank() }?.let { ",$it" }}"
        }
}