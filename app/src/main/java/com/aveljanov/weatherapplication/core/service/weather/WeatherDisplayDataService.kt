package com.aveljanov.weatherapplication.core.service.weather

import android.graphics.Bitmap
import com.aveljanov.weatherapplication.core.model.WeatherResponse
import com.aveljanov.weatherapplication.ui.current_weather.model.WeatherDisplayModel

/**
 * Service for converting the [WeatherResponse] to [WeatherDisplayModel].
 */
interface WeatherDisplayDataService {

    /**
     * Converts [WeatherResponse] to [WeatherDisplayModel].
     */
    suspend fun convertToDisplayDataModel(
        weatherResponse: WeatherResponse?,
        iconBitmap: Bitmap?
    ): WeatherDisplayModel?

}
