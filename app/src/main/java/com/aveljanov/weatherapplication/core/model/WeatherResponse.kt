package com.aveljanov.weatherapplication.core.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Keep
@Serializable
data class WeatherResponse(
    val coord: Coordinates? = null,
    val weather: List<WeatherCondition>? = null,
    val base: String? = null,
    val main: MainWeatherValues? = null,
    val visibility: Long? = null,
    val wind: Wind? = null,
    val clouds: Clouds? = null,
    val rain: Rain? = null,
    val snow: Snow? = null,
    val dt: Long? = null,
    val sys: Sys? = null,
    val timezone: Double? = 0.0,
    val id: Long? = null,
    val name: String? = null,
    val cod: Int? = null
)

@Keep
@Serializable
data class Coordinates(
    val lat: Double,
    val lon: Double
)

@Keep
@Serializable
data class WeatherCondition(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

@Keep
@Serializable
data class MainWeatherValues(
    val temp: Float? = 0f,
    @SerialName("feels_like")
    val feelsLike: Float? = 0f,
    @SerialName("temp_min")
    val tempMin: Float? = 0f,
    @SerialName("temp_max")
    val tempMax: Float? = 0f,
    val pressure: Int? = 0,
    val humidity: Int? = 0,
    @SerialName("sea_level")
    val seaLevel: Int? = 0,
    @SerialName("grnd_level")
    val groundLevel: Int? = 0
)

@Keep
@Serializable
data class Wind(
    val speed: Float? = 0f,
    val deg: Int? = 0,
    val gust: Float? = 0f
)

@Keep
@Serializable
data class Clouds(
    val all: Int
)

@Keep
@Serializable
data class Rain(
    @SerialName("1h")
    val oneHour: Float? = 0f,
    @SerialName("3h")
    val threeHours: Float? = 0f
)

@Keep
@Serializable
data class Snow(
    @SerialName("1h")
    val oneHour: Float? = 0f,
    @SerialName("3h")
    val threeHours: Float? = 0f
)

@Keep
@Serializable
data class Sys(
    val type: Int? = 0,
    val id: Int? = 0,
    val message: Double? = 0.0,
    val country: String? = "",
    val sunrise: Long? = 0,
    val sunset: Long? = 0
)