package com.aveljanov.weatherapplication.core.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.aveljanov.weatherapplication.core.service.result.AppResult
import okhttp3.Response

inline fun api23(func: () -> Unit): Boolean? {

    return Build.VERSION.SDK_INT.compareTo(Build.VERSION_CODES.M).takeIf { it > 0 }?.let {
        func()
        true
    }
}

inline fun Boolean.isTrue(func: () -> Unit): Boolean {
    this.takeIf { it }?.let { func() }
    return this
}

inline fun Boolean.isFalse(func: () -> Unit): Boolean {
    this.takeIf { !it }?.let { func() }
    return this
}

inline fun Activity.hideKeyboard(view: View) {
    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
        view.windowToken,
        InputMethodManager.HIDE_IMPLICIT_ONLY
    )
}

inline fun View.show() {
    visibility = View.VISIBLE
}

inline fun View.hide() {
    visibility = View.GONE
}

@SuppressLint("NewApi")
inline fun ConnectivityManager.hasConnection(): Boolean {
    var hasConnection = false
    api23 {
        val capabilities = this.getNetworkCapabilities(activeNetwork)
        hasConnection =
            capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
    } ?: let {
        hasConnection = this.activeNetworkInfo?.let { it.isAvailable && it.isConnected } ?: false
    }
    return hasConnection
}

suspend inline fun safeCall(function: () -> Response): AppResult<Response> {
    return try {
        AppResult.Success(function())
    } catch (e: Exception) {
        AppResult.Failure(e)
    }
}