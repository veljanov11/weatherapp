package com.aveljanov.weatherapplication.core.service.weather

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.aveljanov.weatherapplication.core.config.AppConfig
import com.aveljanov.weatherapplication.core.model.DeviceLocation
import com.aveljanov.weatherapplication.core.model.WeatherResponse
import com.aveljanov.weatherapplication.core.service.backend.BackendClientService
import com.aveljanov.weatherapplication.core.service.backend.Params
import com.aveljanov.weatherapplication.core.service.result.AppResult
import com.aveljanov.weatherapplication.core.service.units.UnitsPrefsStorage
import com.aveljanov.weatherapplication.core.util.logger
import com.aveljanov.weatherapplication.core.util.safeCall
import kotlinx.serialization.json.Json
import okhttp3.Response
import java.io.IOException

class WeatherServiceImpl(
    private val backendClientService: BackendClientService,
    private val unitsPrefsStorage: UnitsPrefsStorage
) : WeatherService {

    companion object {
        private val logger = logger<WeatherServiceImpl>()
    }

    override suspend fun getWeatherForCity(cityName: String): AppResult<WeatherResponse> {
        logger.info("Get weather for city: $cityName")
        val units = unitsPrefsStorage.getUnits().name
        val response =
            backendClientService.get(
                "data/2.5/weather",
                mapOf(Params.CITY to cityName, Params.UNITS to units)
            )
        return handleResponse(response)
    }

    override suspend fun getWeatherForLocation(deviceLocation: DeviceLocation): AppResult<WeatherResponse> {
        logger.info("Get weather for location: $deviceLocation")
        val units = unitsPrefsStorage.getUnits().name
        return when (val result = safeCall {
            backendClientService.get(
                "data/2.5/weather",
                mapOf(
                    Params.LATITUDE to deviceLocation.lat,
                    Params.LONGITUDE to deviceLocation.lon,
                    Params.UNITS to units
                )
            )
        }) {
            is AppResult.Success -> handleResponse(result.data)
            is AppResult.Failure -> result
        }
    }

    override suspend fun getIcon(iconId: String?): Bitmap? {
        logger.info("Get icon: $iconId")
        return when (val result =
            safeCall { backendClientService.get(url = "${AppConfig.ICON_URL}${iconId!!}@4x.png") }) {
            is AppResult.Success -> {
                val response = result.data
                response?.takeIf { it.isSuccessful }
                    ?.let { BitmapFactory.decodeStream(it.body?.byteStream()) }
            }
            is AppResult.Failure -> null
        }
    }


    private fun handleResponse(response: Response?): AppResult<WeatherResponse> {
        return response?.let {
            response.isSuccessful?.takeIf { it }?.let {
                response.body?.let {
                    val weatherResponse =
                        Json.decodeFromString(WeatherResponse.serializer(), it.string())
                    AppResult.Success(weatherResponse)
                } ?: AppResult.Success(null)
            } ?: AppResult.Failure(IOException(response.message))
        } ?: AppResult.Success(null)
    }
}