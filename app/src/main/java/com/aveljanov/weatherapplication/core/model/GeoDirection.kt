package com.aveljanov.weatherapplication.core.model

import androidx.annotation.Keep

@Keep
enum class GeoDirection {
    W,
    E,
    S,
    N,
    NW,
    NE,
    SW,
    SE
}