package com.aveljanov.weatherapplication.core.service.result

sealed class AppResult<out T> {
    class Success<out T>(val data: T?) : AppResult<T>()
    class Failure(val throwable: Throwable? = null) : AppResult<Nothing>()
}