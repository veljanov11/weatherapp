package com.aveljanov.weatherapplication.core.service.stringprovider

interface StringProviderService {

    /**
     * Provides resource string for given [key].
     */
    fun getString(key: String): String?
}
