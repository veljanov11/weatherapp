package com.aveljanov.weatherapplication.core.service.location

import com.aveljanov.weatherapplication.core.model.DeviceLocation

interface LocationService {

    fun getCurrentLocation(onLocation : (location: DeviceLocation?) -> Unit)
}