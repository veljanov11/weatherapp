package com.aveljanov.weatherapplication.core.config

/**
 * App configuration.
 */
object AppConfig {

    /**
     * Base url for the network calls.
     */
    val BASE_URL = "https://api.openweathermap.org/"
    val ICON_URL = "https://openweathermap.org/img/wn/"
    val API_KEY = "39bbd6ccd17920d097dba0644a0dd844"
}