package com.aveljanov.weatherapplication.core.util

import com.aveljanov.weatherapplication.core.model.GeoDirection
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger

/**
 * Convert [Date]  to "HH:mm" string.
 */
inline fun convertToTimeString(date: Date?): String? = date?.let {
    val threadLocal = ThreadLocal<SimpleDateFormat>()
    threadLocal.set(SimpleDateFormat("HH:mm", Locale.getDefault()))
    threadLocal.get()?.format(date)
}


inline fun convertUTCMillsToDate(utcSeconds: Long?, timezoneOffsetSeconds: Long?): Date? =
    utcSeconds?.let { time ->
        timezoneOffsetSeconds?.let { offset ->
            Date(time.times(1000) + offset.times(1000))
        }
    }

/**
 * Converts circle degrees 0-360  into [GeoDirection].
 */
fun convertDegreesToGeoDirection(degrees: Int?): GeoDirection? = degrees?.let {
    when (it) {
        0 -> GeoDirection.E
        in 0..89 -> GeoDirection.NE
        90 -> GeoDirection.N
        in 90..179 -> GeoDirection.NW
        180 -> GeoDirection.W
        in 180..269 -> GeoDirection.SW
        270 -> GeoDirection.S
        in 270..359 -> GeoDirection.SE
        else -> null
    }
}

/**
 * Construct url "[baseUrl]/[path]?[params]"
 */
fun constructUrl(baseUrl: String, path: String, params: Map<String, Any>?): String {
    return "${baseUrl.removeTrailingSlash()}/${path.removeTrailingSlash()}?${
    params?.entries?.fold("") { acc, entry -> acc.plus("${entry.key}=${entry.value}&") }
    }"
}

inline fun String.removeTrailingSlash(): String {
    return takeIf { last() == '/' }?.let {
        substring(0, length - 1)
    } ?: this
}

/**
 * Get logger for [T].
 */
inline fun <reified T> logger() =
    Logger.getLogger(T::class.simpleName)
