package com.aveljanov.weatherapplication.core.service.backend

import com.aveljanov.weatherapplication.core.util.constructUrl
import com.aveljanov.weatherapplication.core.util.logger
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

class BackendClientService(
    private val okHttpClient: OkHttpClient,
    private val baseUrl: String,
    private val apiKey: String
) {

    companion object {
        private val logger = logger<BackendClientService>()
    }

    suspend fun get(path: String, params: Map<String, Any>? = null): Response {
        val request = createGetRequest(path, params)
        logger.info("Make request: $request")
        return okHttpClient.newCall(request).execute()
    }

    suspend fun get(url: String): Response {
        val request = createGetRequest(url)
        logger.info("Make request: $request")
        return okHttpClient.newCall(request).execute()
    }

    private fun createGetRequest(
        path: String,
        params: Map<String, Any>?
    ): Request {
        val finalParams = params?.let { it.toMutableMap().apply { this[Params.API_KEY] = apiKey } }
            ?: mapOf<String, Any>(Params.API_KEY to apiKey)
        val url = constructUrl(baseUrl, path, finalParams)
        return createGetRequest(url)
    }

    private fun createGetRequest(url: String): Request {
        return Request.Builder()
            .url(url)
            .get().build()
    }
}