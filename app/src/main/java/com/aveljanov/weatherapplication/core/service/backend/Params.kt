package com.aveljanov.weatherapplication.core.service.backend

object Params {

    val CITY = "q"
    val API_KEY = "appid"
    val UNITS = "units"
    val LATITUDE = "lat"
    val LONGITUDE = "lon"

}