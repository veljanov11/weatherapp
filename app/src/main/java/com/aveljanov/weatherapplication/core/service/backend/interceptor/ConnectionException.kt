package com.aveljanov.weatherapplication.core.service.backend.interceptor

import java.io.IOException

class ConnectionException(override val message: String) : IOException(message)