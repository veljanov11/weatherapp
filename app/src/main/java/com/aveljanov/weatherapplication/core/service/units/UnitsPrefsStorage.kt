package com.aveljanov.weatherapplication.core.service.units

import com.aveljanov.weatherapplication.core.model.Units

interface UnitsPrefsStorage {

    suspend fun getUnits(): Units

    suspend fun setUnits(units: Units)

}