package com.aveljanov.weatherapplication.core.model

data class DeviceLocation (
    val lat: Double,
    val lon: Double
)