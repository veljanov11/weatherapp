package com.aveljanov.weatherapplication.core.service.weather

import android.graphics.Bitmap
import com.aveljanov.weatherapplication.core.model.DeviceLocation
import com.aveljanov.weatherapplication.core.model.WeatherResponse
import com.aveljanov.weatherapplication.core.service.result.AppResult

interface WeatherService {

    suspend fun getWeatherForCity(cityName: String): AppResult<WeatherResponse>

    suspend fun getWeatherForLocation(deviceLocation: DeviceLocation): AppResult<WeatherResponse>

    suspend fun getIcon(iconId: String?): Bitmap?

}