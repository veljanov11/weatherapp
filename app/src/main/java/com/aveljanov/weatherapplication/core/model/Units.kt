package com.aveljanov.weatherapplication.core.model

import androidx.annotation.Keep

@Keep
enum class Units {
    METRIC,
    IMPERIAL
}