package com.aveljanov.weatherapplication.core.service.stringprovider

import android.content.Context
import com.aveljanov.weatherapplication.R

class StringProviderServiceImpl(private val context: Context) : StringProviderService {

    companion object {
        const val CHECK_NETWORK_CONNECTION = "network.connection"
        const val CANNOT_REACH_SERVER = "cannot.reach.serv"
    }

    override fun getString(key: String): String? {
        return when (key) {
            CHECK_NETWORK_CONNECTION -> context.getString(R.string.check_network_connection)
            CANNOT_REACH_SERVER -> context.getString(R.string.cannot_reach_server)
            else -> null
        }
    }
}
