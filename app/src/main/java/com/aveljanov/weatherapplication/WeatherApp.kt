package com.aveljanov.weatherapplication

import android.app.Application
import com.aveljanov.weatherapplication.di.appModule
import com.aveljanov.weatherapplication.di.serviceModule
import com.aveljanov.weatherapplication.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.koinApplication

class WeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            modules(
                listOf(
                    appModule,
                    serviceModule,
                    viewModelModule
                )
            )
        }
    }
}