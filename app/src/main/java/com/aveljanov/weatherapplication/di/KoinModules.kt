package com.aveljanov.weatherapplication.di

import android.content.Context
import android.net.ConnectivityManager
import com.aveljanov.weatherapplication.core.config.AppConfig
import com.aveljanov.weatherapplication.core.service.backend.BackendClientService
import com.aveljanov.weatherapplication.core.service.backend.interceptor.ConnectionCheckInterceptor
import com.aveljanov.weatherapplication.core.service.location.LocationService
import com.aveljanov.weatherapplication.core.service.location.LocationServiceImpl
import com.aveljanov.weatherapplication.core.service.stringprovider.StringProviderService
import com.aveljanov.weatherapplication.core.service.stringprovider.StringProviderServiceImpl
import com.aveljanov.weatherapplication.core.service.units.UnitsPrefsStorage
import com.aveljanov.weatherapplication.core.service.units.UnitsPrefsStorageImpl
import com.aveljanov.weatherapplication.core.service.weather.WeatherDisplayDataService
import com.aveljanov.weatherapplication.core.service.weather.WeatherDisplayDataServiceImpl
import com.aveljanov.weatherapplication.core.service.weather.WeatherService
import com.aveljanov.weatherapplication.core.service.weather.WeatherServiceImpl
import com.aveljanov.weatherapplication.ui.current_weather.CurrentWeatherViewModel
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {

    factory { androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    single {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val connectionCheckInterceptor = ConnectionCheckInterceptor(get(), get())
        OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(connectionCheckInterceptor)
            .build() as OkHttpClient
    }

    single {
        LocationServices.getFusedLocationProviderClient(androidContext())
    }

    single {
        androidContext().getSharedPreferences("default", Context.MODE_PRIVATE)
    }
}

val serviceModule = module {
    single { StringProviderServiceImpl(get()) as StringProviderService }
    single { BackendClientService(get(), AppConfig.BASE_URL, AppConfig.API_KEY) }
    single { UnitsPrefsStorageImpl(get()) as UnitsPrefsStorage }
    single { WeatherDisplayDataServiceImpl(get()) as WeatherDisplayDataService }
    single { WeatherServiceImpl(get(), get()) as WeatherService }
    single { LocationServiceImpl(get()) as LocationService }
}

val viewModelModule = module {
    viewModel {
        CurrentWeatherViewModel(
            CoroutineScope(Dispatchers.Main),
            CoroutineScope(Dispatchers.IO),
            get(),
            get(),
            get(),
            get()
        )
    }
}